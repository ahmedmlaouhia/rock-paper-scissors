#include <stdlib.h>
#include <compare_choices.h>

int compareChoices(int playerChoice, int computerChoice) {
    if (playerChoice == computerChoice) {
        return 0; // Draw
    } else if ((playerChoice == 1 && computerChoice == 3) ||
               (playerChoice == 2 && computerChoice == 1) ||
               (playerChoice == 3 && computerChoice == 2)) {
        return 1; // Player wins
    } else {
        return -1; // Computer wins
    }
}
