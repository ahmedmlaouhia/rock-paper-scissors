#ifndef RANDOM_NUMBER_GENERATOR_H
#define RANDOM_NUMBER_GENERATOR_H

int generateRandomNumber(int min, int max);

#endif // RANDOM_NUMBER_GENERATOR_H
