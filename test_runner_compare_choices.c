#include "unity.h"
#include "compare_choices.h"

void setUp(void) {
    // Set up any necessary resources before each test
}

void tearDown(void) {
    // Clean up any resources after each test
}

int main(void) {
    UNITY_BEGIN();
    RUN_TEST(test_compare_choices_draw);
    RUN_TEST(test_compare_choices_player_wins);
    RUN_TEST(test_compare_choices_computer_wins);
    UNITY_END();
}
