#include <stdio.h>
#include "compare_choices.h"
#include "random_number_generator.h"

void test_integration_scenario_1(void) {
    // Test scenario 1
    int playerChoice = 1; // Rock
    int computerChoice = 3; // Scissors

    int result = compareChoices(playerChoice, computerChoice);
    if (result == 1) {
        printf("Integration Test Scenario 1 Passed\n");
    } else {
        printf("Integration Test Scenario 1 Failed\n");
    }
}

void test_integration_scenario_2(void) {
    // Test scenario 2
    int playerChoice = 2; // Paper
    int computerChoice = 2; // Paper

    int result = compareChoices(playerChoice, computerChoice);
    if (result == 0) {
        printf("Integration Test Scenario 2 Passed\n");
    } else {
        printf("Integration Test Scenario 2 Failed\n");
    }
}

void test_integration_scenario_3(void) {
    // Test scenario 3
    int playerChoice = 3; // Scissors
    int computerChoice = 1; // Rock

    int result = compareChoices(playerChoice, computerChoice);
    if (result == -1) {
        printf("Integration Test Scenario 3 Passed\n");
    } else {
        printf("Integration Test Scenario 3 Failed\n");
    }
}

void setUp(void) {
    // Set up any necessary resources before each test
}

void tearDown(void) {
    // Clean up any resources after each test
}

int main(void) {
    test_integration_scenario_1();
    test_integration_scenario_2();
    test_integration_scenario_3();

    return 0;
}
