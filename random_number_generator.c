#include <stdlib.h>
#include "random_number_generator.h"

int generateRandomNumber(int min, int max) {
    return rand() % (max - min + 1) + min;
}
