#include "unity.h"
#include "compare_choices.h"

void test_compare_choices_draw(void) {
    int result = compareChoices(1, 1);
    TEST_ASSERT_EQUAL_INT(0, result);
    result = compareChoices(2, 2);
    TEST_ASSERT_EQUAL_INT(0, result);
    result = compareChoices(3, 3);
    TEST_ASSERT_EQUAL_INT(0, result);
}

void test_compare_choices_player_wins(void) {
    int result = compareChoices(1, 3);
    TEST_ASSERT_EQUAL_INT(1, result);
    result = compareChoices(2, 1);
    TEST_ASSERT_EQUAL_INT(1, result);
    result = compareChoices(3, 2);
    TEST_ASSERT_EQUAL_INT(1, result);

}

void test_compare_choices_computer_wins(void) {
    int result = compareChoices(3, 1);
    TEST_ASSERT_EQUAL_INT(-1, result);
    result = compareChoices(1, 2);
    TEST_ASSERT_EQUAL_INT(-1, result);
    result = compareChoices(2, 3);
    TEST_ASSERT_EQUAL_INT(-1, result);
}

void setUp(void) {
    // Set up any necessary resources before each test
}

void tearDown(void) {
    // Clean up any resources after each test
}

int main(void) {
    UNITY_BEGIN();
    RUN_TEST(test_compare_choices_draw);
    RUN_TEST(test_compare_choices_player_wins);
    RUN_TEST(test_compare_choices_computer_wins);
    UNITY_END();
}