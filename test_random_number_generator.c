#include "unity.h"
#include "random_number_generator.h"

void setUp(void) {
    // Set up any necessary resources before each test
}

void tearDown(void) {
    // Clean up any resources after each test
}

void test_generate_random_number(void) {
    int min = 5;
    int max = 10;
    int result = generateRandomNumber(min, max);

    TEST_ASSERT_TRUE(result >= min && result <= max);
}

int main(void) {
    UNITY_BEGIN();
    RUN_TEST(test_generate_random_number);
    UNITY_END();
}
